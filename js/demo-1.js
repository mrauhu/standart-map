/* 
 * Demo 1
 * - демонстрация работы с API Яндекс.Карт 2.0
 */

// Объект демонстрации
var d = {
	m : '',		// Карта
	с : ''		// Коллекция геобъектов
};
var conf = {
	'geo' : [54.7384, 55.9832 ]
};

// Как только подгрузятся Яндекс.Карты
ymaps.ready( function(){
	// Инициализируем карту
	d.m = new ymaps.Map( 'map-demo-1', {
		center: conf.geo,
		zoom: 12,
		behaviors: ["default", "scrollZoom"]
	});
// Debug route build
/*
	build (	[	// Геометрия
		[54.71726, 56.00403], [54.73528,55.99238], [54.74137, 55.96794], [54.76379, 56.00319]
		], {	// Свойства
			balloonContent: 'Route'	// cодержимое балуна при клике на линию
		}, {	// Опции
			strokeWidth: 2,			// толщина линии
			strokeOpacity: 0.5,		// прозрачность линии
			strokeColor: "#0974e3"	// цвет
		}, [ 'alpha', 'beta', 'gamma', 'delta' ]	// Названия
	);
	*/
});

// Построение маршрута
function build ( geometry, properties, options, names ) {
	// Значения по умолчанию
	properties = ( typeof properties !== 'undefined' ) ? properties : '';
	options = ( typeof options !== 'undefined' ) ? options : '';
	names	= ( Object.prototype.toString.call( names ) === '[object Array]' )
	? names
	: false;

	var geo = {	// для автомаштабирования
		min : {
			x : geometry[0][0],
			y : geometry[0][1]
		},
		max : {
			x : 0,
			y : 0
		}
	},
	points = geometry.length,	// количество узлов
	placemark;					// метка

	// Проверка, если объекты в коллекции
	if ( typeof( d.c ) !== 'object' ) {
		d.c = new ymaps.GeoObjectCollection();	// Создаем коллекцию
	} else {
		d.c.removeAll();						// Удаляем все объекты
	}
	// Создаем ломанную
	// http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/Polyline.xml
	var polyline = new ymaps.Polyline( geometry, properties, options );
	d.c.add( polyline );

	// Обход всех узлов ломанной
	for( var i = 0, x, y; i < points; i++ ) {
		x = geometry[i][0];
		y = geometry[i][1];
		// Создаем балуны в ломанной
		placemark = new ymaps.Placemark( geometry[ i ], {
			// Свойства
			// Вывод № объекта, или если ничего не задано - порядковый
			iconContent: ( names ) ? names[ i ] : ( i + 1 )
		}, {
			// Опции
			preset: 'twirl#blueStretchyIcon' // иконка растягивается под контент
		});
		d.c.add( placemark );
		// Находим левый нижний и правый верхний угол
		geo.min.x = ( geo.min.x > x ) ? x : geo.min.x;
		geo.min.y = ( geo.min.y > y ) ? y : geo.min.y;
		geo.max.x = ( geo.max.x < x ) ? x : geo.max.x;
		geo.max.y = ( geo.max.y < y ) ? y : geo.max.y;
	}

	// Добавляем коллекцию к карте
	d.m.geoObjects.add( d.c );
	// Автомаштабирование карты
	d.m.setBounds( [ [ geo.min.x, geo.min.y ], [ geo.max.x, geo.max.y ] ], {
		checkZoomRange: true,
		duration: 1000
	});

// Перемещаем карту в первую точку маршрута
/*d.m.panTo( geometry[0], { // при включении нет эффекта от автомаштабирования
		flying: true,
		duration: 1000
	});*/
}

