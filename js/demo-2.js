var map = '',
last_id = 0,
conf = {
	geo : [54.7384, 55.9832 ],
	zoom: 14
},
points = [],
line = ''; 


// Как только подгрузятся Яндекс.Карты
ymaps.ready( function(){
	// Инициализируем карту
	map = new ymaps.Map( 'map-demo-1', {
		center: conf.geo,
		zoom: conf.zoom,
		behaviors: ["default", "scrollZoom"]
	});
});

// Установка центра карты
function set_config ( config ) {
	conf = config;
	map.panTo( conf.geo, { // при включении нет эффекта от автомаштабирования
		flying: true,
		duration: 1000
	});
}

function update_line( ) {
	// Добавим точки во временный массив
	var geo = [];
	for (var id in points) {
		if ( points[ id ].geo !== 'undefined' ) {
			geo.push( points[ id ].geo );
		}
	}
	if ( geo.length > 1) {
		// Проверим существует ли линия на карте
		if ( typeof line === 'object') {
			line.geometry.setCoordinates( geo );	// Обновим значения старой
		} else {
			line = new ymaps.Polyline( geo );		// Создадим новую линию
			map.geoObjects.add ( line );
		}
		return true;
	}
	return false;
}

// Создание / отрисовка точки
function add_point ( geo, name, type, radius, draggable ) {
	var id = ++last_id,
	preset = 'twirl#greyStretchyIcon';	// вид иконки
	
	// Значения по умолчанию
	geo = ( ( typeof geo !== 'undefined' ) && ( geo ) // Установим позицию точки
		&& ( geo.length > 0 ) ) ? geo : conf.geo;
	name = ( typeof name !== 'undefined' ) ? name : '';
	switch ( type ) {
		case 1:
			preset = 'twirl#darkorangeStretchyIcon';
			break;
		case 2:
			preset = 'twirl#lightblueStretchyIcon';
		default:
			type = 0;
			break;
	}
	radius = ( typeof radius !== 'undefined' ) ? radius : 50;
	draggable = ( typeof draggable !== 'undefined') ? draggable : true;

	// Единое хранилище
	points [ id ] = {
		'geo'	: geo,
		'name'	: name,
		'type'	: type,
		'radius': radius,
		'draggable' : draggable
	}

	// Создаем круг
	var circle = new ymaps.Circle([
		geo,	// Координаты центра круга
		radius		// Радиус круга в метрах
		]),
	// Метку	
	mark = new ymaps.Placemark(
		geo, {
			hintContent: 'Двойной щелчок показывает меню',
			iconContent: name
		}, {
			draggable: draggable,// Перемещение
			preset: preset		// Иконка
		});
	// Линию
	update_line();

	// При отпускании метки	
	mark.events.add('dragend', function (e) {
		geo = mark.geometry.getCoordinates();
		map.hint.show( geo,
			'Позиция: ' + geo );
		// Обновим позицию точки
		points[ id ].geo = geo;
		circle.geometry.setCoordinates( geo );
		update_line();
	});
	
	// При нажатии на метку
	mark.events.add('dblclick', function (e) {
		// Отключаем стандартное контекстное меню браузера
		e.get('domEvent').callMethod('preventDefault');
		// Если меню метки уже отображено, то убираем его при повторном нажатии правой кнопкой мыши 
		if ($('#menu').css('display') == 'block') {
			$('#menu').remove();
		} else {
			// HTML-содержимое контекстного меню.
			var menuContent =
			'<div id="menu">\
                             <form><ul id="menu_list">\
                                 <li>Название: <br /> <input type="text" size = "10" name="icon_text" /></li>\
                                 <li>Радиус, м: <br /> <input type="text" size = "10" name="radius" /></li>\
                             </ul>\
                         <div align="center"><input type="submit" value="Сохранить" /></div>\
                         </form></div>';
			// Размещаем контекстное меню на странице
			$('body').append(menuContent);

			// ... и задаем его стилевое оформление.
			$('#menu').css({
				position: 'absolute',
				left: e.get('position')[0],
				top: e.get('position')[1],
				background: '#F8F8F8',
				border: '1px solid #B37A9E',
				'border-radius': '12px',
				width: 'auto',
				'padding-bottom': '10px',
				'z-index': 2
			});

			$('#menu ul').css({
				'list-style-type': 'none',
				padding: '20px',
				margin: 0
			});

			// Заполняем поля контекстного меню текущими значениями свойств метки.
			$('#menu input[name="icon_text"]').val(mark.properties.get('iconContent'));
			$('input[name="radius"]').val( circle.geometry.getRadius( ));

			// При нажатии на кнопку "Сохранить" изменяем свойства метки
			// значениями, введенными в форме контекстного меню.
			$('#menu input[type="submit"]').click(function ( event ) {
				event.preventDefault();
				var iconText = $('input[name="icon_text"]').val(),
				radius = $('input[name="radius"]').val();
				circle.geometry.setRadius( radius );
				mark.properties.set({
					iconContent: iconText
				});
				points[ id ].name = iconText;	// Обновим значение имени
				points[ id ].radius = radius;
				
				// Удаляем контекстное меню.
				$('#menu').remove();
			});
		}
		
	});

	try {
		map.geoObjects.add ( circle );
		map.geoObjects.add ( mark );
	} catch (exception) { 
		//console.log('error adding geoObjects on map');
	}

	return id;
}

// Получение точки
function get_point ( id, return_string ) {
	
	id = ( typeof  id !== 'undefined' ) ? id : last_id; 
	// По умолчанию выводим значение строкой
	return_string = ( typeof  return_string === 'undefined' ) ? true : return_string; 
	
	if ( typeof points[ id ] !== 'undefined' ) {
		if ( return_string ) {
			var p = points [ id ];
			// geo[0], geo[1], name, type, radius
			return p['geo'][0] + ', ' + p['geo'][1] + ', "'
			+ p['name'] + '", ' + p['type'] + ', ' + p['radius'];
		}
		return [ id, points [ id ] ];
	}
	
	return false;

}

// Удаление точки
function delete_point ( id ) {
	id = ( typeof id !== 'undefined' ) ? id : last_id;
	
	if ( typeof points[ id ] !== 'undefined' ) {
		delete points[ id ];
		return true;
	}
	return false
}

// Функция создания полигона
function draw_poly ( coordinates, properties, options ) {
	// Значения по умолчанию
	properties = ( typeof properties !== 'undefined' ) ? properties : {
		hintContent: "Polygon"
	};
	options = ( typeof id !== 'undefined' ) ? options : {
		fillColor: '#75ff88',
		strokeWidth: 3,
		opacity: 0.5
	};

	
	var polygon = new ymaps.Polygon ( coordinates, properties, options );
	try {
		map.geoObjects.add ( polygon );
	} catch (exception) { 
		//console.log('error adding polygon on map ', exception);
	}

}
